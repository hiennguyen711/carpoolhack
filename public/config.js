'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'carpool';

	//dependencies injection
	var applicationModuleVendorDependencies = ['ngResource', 'ui.router', 'ui.bootstrap', 'ui.utils'];




//register modules
	var registerModule = function(moduleName, dependencies) {
	//module name
		angular.module(moduleName, dependencies || []);

	//add the module to the main file 
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
