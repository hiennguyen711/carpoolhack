'use strict';

angular.module('about').controller('AboutController', ['$scope',
	function($scope) {
		// About controller logic
		// ...
        $scope.title = "About CarPool";
	}
]);