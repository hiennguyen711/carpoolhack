'use strict';

angular.module('contacts').controller('ContactsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Contacts',
	function($scope, $stateParams, $location, Authentication, Contacts) {
		$scope.authentication = Authentication;
//create new contact
		$scope.create = function() {
			var contact = new Contacts({
				title: this.title,
				content: this.content
			});
			//sage
			contact.$save(function(response) {
				$location.path('contacts/' + response._id);
						var $toastContent = $('<span>Successfully create new contact</span>');
  Materialize.toast($toastContent, 5000);

				$scope.title = '';
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
//remove one contact
		$scope.remove = function(contact) {
			if (contact) {
				contact.$remove();

				for (var i in $scope.contacts) {
					if ($scope.contacts[i] === contact) {
						$scope.contacts.splice(i, 1);
					}
				}
			} else {
				$scope.contact.$remove(function() {
					$location.path('contacts');
							var $toastContent = $('<span>Successfully remove the contact</span>');
  Materialize.toast($toastContent, 5000);
				});
			}
		};
//update on contact
		$scope.update = function() {
			var contact = $scope.contact;

			contact.$update(function() {
				$location.path('contacts/' + contact._id);
						var $toastContent = $('<span>Successfully update the contact</span>');
  Materialize.toast($toastContent, 5000);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
//list all contacts
		$scope.find = function() {
			$scope.contacts = Contacts.query();
		};
//find one contact
		$scope.findOne = function() {
			$scope.contact = Contacts.get({
				contactId: $stateParams.contactId
			});
		};
	}
]);