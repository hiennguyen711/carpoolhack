'use strict';

//Needs service used to communicate Needs REST endpoints
angular.module('needs').factory('Needs', ['$resource',function($resource) {
	//post new need 
		return $resource('needs/:needId', { needId: '@_id'}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);