'use strict';

// Configuring the contacts module
angular.module('needs').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Carpool NeewFeeds', 'needs', 'dropdown', '/needs(/create)?', true, ['*'], 3);
		Menus.addSubMenuItem('topbar', 'needs', 'Need newsfeed', 'needs', null, true);
		Menus.addSubMenuItem('topbar', 'needs', 'New Need', 'needs/create', null, false, ['admin']);
	}
]);
