'use strict';

//Needs service used to communicate Needs REST endpoints
angular.module('needs').directive('star', ['$resource',function($resource) {
	//post new need 
		return $resource('needs/:needId', { needId: '@_id'}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);