'use strict';

// Needs controller
angular.module('needs').controller('NeedsController', ['$scope', '$stateParams', '$location', '$http', 'Authentication', 'Needs',
	function($scope, $stateParams, $location, $http, Authentication, Needs) {
		//authen 
		$scope.authentication = Authentication;

		// Create new Need
		/**
		 * CREATE NEW NEE
		 */
		$scope.create = function() {
			console.log('create new need');
			// Create new Need object
			var need = new Needs ({
				title: this.title,
				description: this.description,
				typeOfTransport:this.typeOfTransport,
				location: this.location,
				numberNeeded: this.numberNeeded,
				startDate: this.startDate,
				endDate: this.endDate,
				isActive: this.isActive,
				from:this.from,
				to:this.to,
				typeOfTrip:this.typeOfTrip

			});

			// Redirect after save => save 
			need.$save(function(response) {
				//direct to the list of needs new feed
				$location.path('needs/' + response._id);
						var $toastContent = $('<span>Successfully create new need post!</span>');
  Materialize.toast($toastContent, 5000);

				// Clear form fields
				$scope.title = '';
				$scope.description = '';
				$scope.organization = '';
				$scope.location = '';
				$scope.numberNeeded = 1;
				$scope.startDate = Date.now;
				$scope.endDate = null;
				$scope.typeOfTransport = '';
				$scope.from = '';
				$scope.to='';
				$scope.typeOfTrip = '';

			}, function(errorResponse) {
				//if err
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove one need

/**
 * REMOVE NEED
 */

// $scope.reserve = function(res){
// 	var userId = localStorage.getItem('user');
// 	var need = $scope.need;
// 	need.people.push({user:userId});
// 	$location.path('/needs+'/needId')

// }



		$scope.remove = function(need) {
			if ( need ) { 
				need.$remove();

				for (var i in $scope.needs) {
					if ($scope.needs [i] === need) {
						$scope.needs.splice(i, 1);
					}
				}
			} else {
				$scope.need.$remove(function() {
					$location.path('needs');//redirect to needs new feed after delete
							var $toastContent = $('<span>Successfully delete the need post</span>');
  Materialize.toast($toastContent, 5000);
				});
			}
		};

		// Update one need by id 
		$scope.update = function() {
			var need = $scope.need;

			need.$update(function() {
				$location.path('needs/' + need._id);
						var $toastContent = $('<span>Successfully update the need post</span>');
  Materialize.toast($toastContent, 5000);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Needs
		$scope.find = function() {
			$scope.needs = Needs.query();

			$scope.signUpData = {
				name: '',
				email: '',
				message: '',
				needTitle: '',
				need_id: ''
			};
		};


//find user by id
		// $scope.findUserById = function(){
		// 	$http.get('/users/:userId', {
		// 		params: {
		// 			provider: provider
		// 		}
		// 	}).success(function(response) {
		// 		// If successful show success message and clear form
		// 		$scope.success = true;
		// 		$scope.user = Authentication.user = response;
		// 	}).error(function(response) {
		// 		$scope.error = response.message;
		// 	});
		// }

		// Find one node by id
		$scope.findOne = function() {
			$scope.need = Needs.get({ 
				needId: $stateParams.needId
			});
		};

		// Sign-up form > check sign up form 
		$scope.signUp = function(need) {
			var frm = $scope.signUpData;
			var msg = frm.name + ' has requested to participate the ride trip for '	+ need.title + ', item: [' + need._id + ']';

			$http.post('/email',{
				name: frm.name,
				email: frm.email,
				message: msg
			}).
			success(function(response) {
				// if success just sent mail 
				console.log('Success! We have sent your email to the driver !');
						var $toastContent = $('<span>We have sent your email to the driver</span>');
  Materialize.toast($toastContent, 5000);

			}).error(function(response) {
				console.log('Something happened! We cannot send your email.');
						var $toastContent = $('<span>Sorry we cannot send your email</span>');
  Materialize.toast($toastContent, 5000);
			});
		};
	}
]);
