'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		

//not found redirect 
		$urlRouterProvider.otherwise('/');

		//home route
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.view.html'
		});
	}
]);