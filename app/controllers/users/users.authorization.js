'use strict';

var _ = require('lodash'),
	mongoose = require('mongoose'),
	User = mongoose.model('User');

/**
 * User middleware = > find user by ID
 */
exports.userByID = function(req, res, next, id) {
	User.findOne({
		_id: id
	}).exec(function(err, user) {
		if (err) return next(err);
		if (!user) return next(new Error('Failed to load User ' + id));
		req.profile = user;//set req.profile = user 
		next();//next midd
	});
};

exports.findUserById = function(req,res){
	 if (!ObjectId.isValid(req.params.userId)) {
	     return res.status(404).json({ message: 'User not found '});
   }
	User.findOne({_id:req.params.userId}).exec(function(err,user){
		if(err){
			res.json({message:'Error'});
		} else {
			res.json(user);
		}
	});
}

//find user by id
// function findUserById(req, res, next) {
//   if (!ObjectId.isValid(req.params.userId)) {
//     return res.status(404).json({ message: 'User not found '});
//   }

//   User.findById(req.params.userId, (err, user) => {//find by id 
//     if (err) {
//       next(err);
//     } else if (user) {
//       //set req.resources = user
//       req.resources.user = user;
//       next();
//     } else {
//       next(new Error('User not found'));
//     }
//   });
// };
/**
 * Require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
	if (!req.isAuthenticated()) {
		return res.status(401).send({
			message: 'User is not logged in.Please log in!'
		});
	}

	next();
};

/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function(roles) {
	var _this = this;

	return function(req, res, next) {
		_this.requiresLogin(req, res, function() {
			if (_.intersection(req.user.roles, roles).length) {
				return next();
			} else {
				return res.status(403).send({
					message: 'User is not authorized'
				});
			}
		});
	};
};