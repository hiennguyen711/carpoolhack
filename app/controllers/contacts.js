'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var	errorHandler = require('./errors');
var	Contact = mongoose.model('Contact');
//import lodash
var	_ = require('lodash');

//create new contact > send to the user owner
exports.create = function(req, res) {
	var contact = new Contact(req.body);
	contact.user = req.user;

	contact.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(contact);//send json of contact back 
		}
	});
};

//list one contact
exports.read = function(req, res) {
	res.json(req.contact);
};

/**
 * Update a contact by id
 */
exports.update = function(req, res) {
	var contact = req.contact;

	contact = _.extend(contact, req.body);

	contact.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(contact);
		}
	});
};

/**
 * Delete an contact
 */
exports.delete = function(req, res) {
	var contact = req.contact;

	contact.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(contact);
		}
	});
};

/**
 * List of contacts > lsit all
 */
exports.list = function(req, res) {
	Contact.find().sort('-created').populate('user', 'displayName').exec(function(err, contacts) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(contacts);
		}
	});
};

/**
 * contact middleware
 */
//find by id
exports.contactByID = function(req, res, next, id) {
	Contact.findById(id).populate('user', 'displayName').exec(function(err, contact) {
		if (err) return next(err);
		if (!contact) return next(new Error('Failed to load contact ' + id));
		req.contact = contact;
		next();
	});
};

/**
 * contact authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.contact.user.id !== req.user.id) {
		return res.status(403).send({
			message: 'User is not authorized'
		});
	}
	next();
};