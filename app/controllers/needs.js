'use strict';

var mongoose = require('mongoose');
var	errorHandler = require('./errors');
var	Need = mongoose.model('Need');
var	_ = require('lodash');

//create new need 
exports.create = function(req, res) {
	var need = new Need(req.body);
	//need.user = req.user;
	need.createdBy = req.user;

	need.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(need);
		}
	});
};

//list one need
exports.read = function(req, res) {
	res.jsonp(req.need);
};

//update one need by id
exports.update = function(req, res) {
	var need = req.need ;

	need = _.extend(need , req.body);

	need.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(need);
		}
	});
};

//delete one need by id
exports.delete = function(req, res) {
	var need = req.need ;

	need.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(need);
		}
	});
};
//list all needss
exports.list = function(req, res) { 
	Need.find().sort('-created').populate('user', 'displayName').exec(function(err, needs) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(needs);
		}
	});
};

//find by id middlewre
exports.needByID = function(req, res, next, id) { 
	Need.findById(id).populate('user', 'displayName').exec(function(err, need) {
		if (err) return next(err);
		if (! need) return next(new Error('Failed to load Need ' + id));
		req.need = need ;
		next();//middleware require
	});
};

//author middleware
exports.hasAuthorization = function(req, res, next) {
	var hasAuth = false;
	var isAdminRole = _.contains(req.user.roles, 'admin');
	
	// Anyone has authorization to their own documents...
	if (req.need.createdBy === req.user.id) {
		hasAuth = true;
	// Admins can edit any data...
	} else if (isAdminRole) { 
		hasAuth = true;
	}
			
	if (!hasAuth) {
		return res.status(403).send('User is not authorized');
	}
	next();
};


//getPeople
exports.createPeople = function(req,res){
	// var need = new Need(req.body);
	// //need.user = req.user;
	// need.createdBy = req.user;

	// need.save(function(err) {
	// 	if (err) {
	// 		return res.status(400).send({
	// 			message: errorHandler.getErrorMessage(err)
	// 		});
	// 	} else {
	// 		res.jsonp(need);
	// 	}
	// });
	req.need.people.push(req.body);
	req.need.save(function(err,updatedNeed){
		if(err){
			res.json({message:'Erorr to fetch the need'});
		} 

		req.need= updatedNeed;
		res.json(req.need);
	})

}


// function updateParticipants(req, res, next) {



// req.resources.event.participants.push(req.body);


//   req.resources.event.save((err, updatedEvent) => {
//     if (err) {
//       return next(err);
//     }

//     // res.json(event);

//     //next()

//     req.resources.event = updatedEvent;
//     next();
//   });


// }
