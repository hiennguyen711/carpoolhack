'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Need Schema
 */
var NeedSchema = new Schema({
	title: {
		type: String,
		default: '',
		required: 'Please provide a title',
		trim: true
	},
	description: {
		type: String,
		default: '',
		required: 'Please provide a description',
		trim: true
	},
	typeOfTransport: {
		type: String,
		default: ''
	},
	location: {
		type: String,
		default: '',		
		trim: true
	},
	startDate: {
		type: Date
		// type:String
	},
	endDate: {
		type: Date
	},
	from:{
		type:String,
		required:true
	
	},
	to:{
		type:String,
		required:true

	},
	typeOfTrip:{
		type:String,
		default:'Round trip'
	},
	numberNeeded: {
		type: Number,
		default: 1
	},
	people: [
		{user: Schema.ObjectId}
	],
	createdOn: {
		type: Date,
		default: Date.now
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdBy: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Need', NeedSchema);
