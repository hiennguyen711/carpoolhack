'use strict';

module.exports = function(app) {
    // Root routing
    var email = require('../../app/controllers/email');
//post email > create with nodemailer
    app.route('/email').post(email.create);

};
