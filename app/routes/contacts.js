'use strict';

var users = require('../../app/controllers/users'),
	contacts = require('../../app/controllers/contacts');

module.exports = function(app) {
	//GET & POST > GET ALL / POST ONE 
	app.route('/contacts')
		.get(contacts.list)
		.post(users.requiresLogin, contacts.create);
//GET PUT DELETE
	app.route('/contacts/:contactId')
		.get(contacts.read)
		.put(users.requiresLogin, contacts.hasAuthorization, contacts.update)
		.delete(users.requiresLogin, contacts.hasAuthorization, contacts.delete);

	// Finish by binding the contact middleware
	app.param('contactId', contacts.contactByID);
};