'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var needs = require('../../app/controllers/needs');

	//list all needs / create one need 
	app.route('/needs')
		.get(needs.list)
		.post(users.requiresLogin, needs.create);


//list one need, edit one need, delete one need
	app.route('/needs/:needId')
		.get(needs.read)
		.put(users.requiresLogin, needs.hasAuthorization, needs.update)
		.delete(users.requiresLogin, needs.hasAuthorization, needs.delete);
//from get needs.read > fetch need obj > include people 

//get the reserve slot 
app.route('/needs/:needId/people')
	
	.post(users.requiresLogin,needs.createPeople)
	// .delete(needs.deletePeople)




	// Finish by binding the Need middleware
	app.param('needId', needs.needByID);
};
