'use strict';

module.exports = {
	app: {
		title: 'Carpool app',
		description: 'Carpool platform app',
		keywords: 'MEAN stack, hackathon'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'MEAN',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				
				'public/theme/css/stylehome.css',
				'public/theme/css/materialdesign.css',
				'http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css',
				'public/theme/css/materialangular.css'
			],
			js: [
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js', 
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'https://code.jquery.com/jquery-3.1.1.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js'
			]
		},
		css: [
			// 'public/modules/**/css/*.css',
            'public/theme/**/css/*.css',
			'http://ajax.googleapis.com/ajax/libs/angular_material/1.1.0/angular-material.min.css',
			'public/theme/css/materialangular.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js',
			'https://code.jquery.com/jquery-3.1.1.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};

