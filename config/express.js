'use strict';

var fs = require('fs');
var	http = require('http');
var chalk = require('chalk');
var	https = require('https');
var	express = require('express');
var	morgan = require('morgan');
var	bodyParser = require('body-parser');
var	session = require('express-session');
var	compress = require('compression');
var	methodOverride = require('method-override');
var	cookieParser = require('cookie-parser');
var	helmet = require('helmet');
var	passport = require('passport');
var	mongoStore = require('connect-mongo')({
		session: session
	});
var	flash = require('connect-flash');
var	config = require('./config');
var	consolidate = require('consolidate');
var	path = require('path');

module.exports = function(db) {
	// Initialize express app
	var app = express();

	// Globbing model files
	config.getGlobbedFiles('./app/models/**/*.js').forEach(function(modelPath) {
		require(path.resolve(modelPath));
	});

	// Setting application local variables
	app.locals.title = config.app.title;
	app.locals.description = config.app.description;

	app.locals.keywords = config.app.keywords;

	app.locals.facebookAppId = config.facebook.clientID;


	app.locals.jsFiles = config.getJavaScriptAssets();
	app.locals.cssFiles = config.getCSSAssets();

	// Passing the request url to environment locals
	app.use(function(req, res, next) {
		res.locals.url = req.protocol + '://' + req.headers.host + req.url;
		next();
	});

	//compress for production 
	app.use(compress({
		filter: function(req, res) {
			return (/json|text|javascript|css/).test(res.getHeader('Content-Type'));
		},
		level: 9
	}));

	// Showing stack errors
	app.set('showStackError', true);

	// Set swig as the template engine
	app.engine('server.view.html', consolidate[config.templateEngine]);

	// Set views path and view engine
	app.set('view engine', 'server.view.html');
	app.set('views', './app/views');

	
	if (process.env.NODE_ENV === 'development') {
		//morgan
		app.use(morgan('dev'));

		
		app.set('view cache', false);
	} else if (process.env.NODE_ENV === 'production') {
		app.locals.cache = 'memory';
	}
//middlware uses
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(bodyParser.json());
	app.use(methodOverride());

	
	app.use(cookieParser());

	// Express MongoDB session storage
	app.use(session({
		saveUninitialized: true,
		resave: true,
		secret: config.sessionSecret,
		store: new mongoStore({
			db: db.connection.db,
			collection: config.sessionCollection
		})
	}));

	// use passport session
	app.use(passport.initialize());

	app.use(passport.session());
//use flash mess
	
	app.use(flash());

	// Use helmet to secure Express headers
	app.use(helmet.xframe());
	app.use(helmet.xssFilter());
	app.use(helmet.nosniff());
	app.use(helmet.ienoopen());
	app.disable('x-powered-by');

	// Setting the app router and static folder
	app.use(express.static(path.resolve('./public')));

	// Globbing routing files
	config.getGlobbedFiles('./app/routes/**/*.js').forEach(function(routePath) {
		require(path.resolve(routePath))(app);
	});

//error middleware
	app.use(function(err, req, res, next) {
		
		if (!err) return next();

		console.error(err.stack);
		// console.log(chalk.red(err));

		// render error page
		res.status(500).render('500', {
			error: err.stack
		});
	});

//404 middleare
	app.use(function(req, res) {
		res.status(404).render('404', {
			url: req.originalUrl,
			error: 'Not Found'
		});
	});

	// if (process.env.NODE_ENV === 'secure') {
	// 	// Log SSL usage
	// 	console.log('Securely using https protocol');

	// 	// Load SSL key and certificate
	// 	var privateKey = fs.readFileSync('./config/sslcerts/key.pem', 'utf8');
	// 	var certificate = fs.readFileSync('./config/sslcerts/cert.pem', 'utf8');

	// 	// Create HTTPS Server
	// 	var httpsServer = https.createServer({
	// 		key: privateKey,
	// 		cert: certificate
	// 	}, app);

	// 	// Return HTTPS server instance
	// 	return httpsServer;
	// }
	


	return app;
};